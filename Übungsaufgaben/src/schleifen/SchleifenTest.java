package schleifen;

import java.util.Scanner;

public class SchleifenTest {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int anzahl;
		System.out.println("Bitte geben Sie eine Zahl ein");
		anzahl = scan.nextInt();
		for (int i = 0; i < anzahl; i++) {
			System.out.print("+");
		}
		System.out.println();
		int i = 0;
		while (i < anzahl) {

			System.out.print("-");
			i++;

		}
		System.out.println();
		int i1 = 0;
		do {
			System.out.print("*");
			i1++;
		} while (i1 < anzahl );

	}
}
